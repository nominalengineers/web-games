// Global variables
var gDocument;

function onButtonClickHandler() {
  var userName = prompt("What is your name stranger ?");
  var result;
  if (confirm("Are you sure ?")) {
    result = "Greetings "+ userName + " !";
  } else {
    result = "Greetings stranger aka "+ userName + " !";
  }//if
  //document.write(result);
  gDocument.getElementById("eventBox").innerHTML = result;
}//onLoadFunc

function init(document) {
  initVariables(document);
  initEventHandlers();
}//eventsInit

function initVariables(document) {
  gDocument = document;
}//initVariables

function initEventHandlers() {
  gDocument.getElementById("button").addEventListener("click", onButtonClickHandler);
}//initEventHandlers
