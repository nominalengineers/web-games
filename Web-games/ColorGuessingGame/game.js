
var gDocument = null;
var riddle = "";
var riddleIndex = -1;
var trials = 0;
var colors = ["aqua","black","blueviolet","brown","coral","cyan","gold","green","lime","navy","orange","salmon","skyblue","violet","white","yellow"];

function init(document) {
  initBase(document);
  initGame();
}//init

function initBase(document) {
  gDocument = document;
  colors = colors.sort();
}//initBase

function initGame() {
  var guessIndex = Math.floor(Math.random() * colors.length);
  riddleIndex = guessIndex;
  riddle = colors[riddleIndex];
  trials = 0;
}//initBase

function doGame(document) {
  init(document);

  // Main game loop
  while(!checkGuess(mainPrompt())) {}

  if (confirm("Would you like to play more?")) {
    doGame(gDocument);
  }//if
}//doGame

function mainPrompt() {
  var greetings = colors[riddleIndex] + "\n\nI am thinking of one of these colors: \n\n" + colors + "\n\n" + "What color am I thinking of ?";
  return prompt(greetings);
}//mainPrompt

function checkGuess(guess) {
  trials++;
  var result = false;
  var guessIndex = colors.indexOf(guess);
  if (guessIndex == -1) {
    alert("Sorry, I don't recognize your color. \n\n Please try again.");
  } else if (guessIndex > riddleIndex) {
    alert("Sorry, your guess is not correct! \n\nHint: your color is alphabetically higher than mine.\n\nPlease try again.");
  }  else if (guessIndex < riddleIndex) {
    alert("Sorry, your guess is not correct! \n\nHint: your color is alphabetically lower than mine.\n\nPlease try again.");
  }  else  {
    gDocument.body.style.background = colors[riddleIndex];
    alert("Congratulations! You have guessed the color! \n\n It took you "+ trials +" guesses to finish the game!\n\n You can see colour in the background");
    result = true;
  }//if

  return result;
}//checkGuess
