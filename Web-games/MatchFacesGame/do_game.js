var gDocument;
var leftGameField = document.getElementById("leftSide");
var rightGameField = document.getElementById("rightSide");
var startBtn = document.getElementById("startButton");
var facesStep = 5;
var currentFaces = 0;
var gameFieldWidth;
var gameFieldHeight;
var imgSize = 50;

function init(document) {
  gDocument = document;
  startBtn.addEventListener("click", function() { startGame();} );
}//init

function startGame() {
  event.stopPropagation();
  startBtn.style.display = "none";

  // Calculate container size
  var gameFieldWidth = parseInt(leftGameField.offsetWidth) - imgSize;
  var gameFieldHeight = parseInt(leftGameField.offsetHeight) - imgSize;

  // Attach listener to body game-fail
  gDocument.body.addEventListener("click", gameOver);

  currentFaces += facesStep;
  // Loop for X faces
  for (var i = 0; i < currentFaces; i++) {

    // Create element face img
    var imgSmile = gDocument.createElement("img");
    imgSmile.src = "smile.png";
    imgSmile.style.position = "absolute";
    imgSmile.style.width = imgSize + "px";
    imgSmile.style.height = imgSize + "px";

    // Append img to leftSide game field
    leftGameField.appendChild(imgSmile);

    // Set random coordinates X (0, div width)
    // Set random coordinates Y (0, div height)
    var randomX = Math.floor(Math.random() * (gameFieldWidth - imgSize)) + imgSize;
    var randomY = Math.floor(Math.random() * (gameFieldHeight - imgSize)) + imgSize;

    imgSmile.style.left = randomX + "px";
    imgSmile.style.top = randomY + "px";
  }//for

  // Get last child of leftFieldField
  leftGameField.lastChild.addEventListener("click", continueGame);

  // Clone all branch
  var clonedBranch = leftGameField.cloneNode(true);

  // Remove last child of rightGameField
  clonedBranch.removeChild(clonedBranch.lastChild);

  // Offset X for clonedBranch
  for(i = 0; i < clonedBranch.childNodes.length ; i++) {
    var node = clonedBranch.childNodes[i];
    if (node.nodeName == "IMG") {
      node.style.left = (parseInt(node.style.left) + gameFieldWidth)+ "px";
    }//if
  }//for

  // Append it to rightGameField
  rightGameField.appendChild(clonedBranch);
}//startGame

function gameOver() {
  event.stopPropagation();

  // Restore variables
  currentFaces = 0;

  // Show correct face
  leftGameField.lastChild.style.width = (imgSize * 2) + "px";
  leftGameField.lastChild.style.height = (imgSize * 2) + "px";

  // Attach listener to body game-fail
  gDocument.body.removeEventListener("click", gameOver);
  alert("You've failed ! \n\n GAME OVER !\n\n Press Start to play again.");
  startBtn.style.display = "block";

  clearField(leftGameField);
  clearField(rightGameField);

}//gameOver

function clearField(elementToClean) {
  while(elementToClean.firstChild) {
    var el = elementToClean.firstChild;
    elementToClean.removeChild(el);
  }//while
}//clearField

function continueGame() {
  event.stopPropagation();
  gDocument.body.removeEventListener("click", gameOver);
  clearField(leftGameField);
  clearField(rightGameField);
  startGame();
}//continueGame
